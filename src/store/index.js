import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import VideoChat from './modules/video-chat'

const store = new Vuex.Store({
    modules: {
        VideoChat
    }
})

export default store