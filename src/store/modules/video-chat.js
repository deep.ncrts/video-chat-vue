export default {
    state: {
        clients: [],
        room: '',
        user: {
            name: '',
            muted: false,
            paused: false,
        }
    },

    getters: {
        getRoom: state => state.room,
        getName: state => state.user,
        getClients: state => state.clients
    },

    mutations: {
        setName: (state, name) => {
            state.user.name = name
        },
        addPeer: (state, { video, peer }) => {
            state.clients.push({ video, peer })
        }
    }
}