import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/room/:room',
    name: 'Room',
    component: () => import(/* webpackChunkName: "about" */ '../views/Room.vue'),
    props: true,
    beforeEnter: (to, from, next) => {
      if (!store.getters.getName.name) {
        next({ name: 'Home', query: { room: to.params.room } })
      }
      next()
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
